fournitures_scolaires = \
[{'Nom' : 'Manuel scolaire', 'Poids' : 0.55, 'Mana' : 11},
{'Nom' : 'Baguette magique', 'Poids' : 0.085, 'Mana' : 120},
{'Nom' : 'Chaudron', 'Poids' : 2.5, 'Mana' : 2},
{'Nom' : 'Boîte de fioles', 'Poids' : 1.2, 'Mana' : 4},
{'Nom' : 'Téléscope', 'Poids' : 1.9, 'Mana' : 6},
{'Nom' : 'Balance de cuivre', 'Poids' : 1.3, 'Mana' : 3},
{'Nom' : 'Robe de travail', 'Poids' : 0.5, 'Mana' : 8},
{'Nom' : 'Chapeau pointu', 'Poids' : 0.7, 'Mana' : 9},
{'Nom' : 'Gants', 'Poids' : 0.6, 'Mana' : 25},
{'Nom' : 'Cape', 'Poids' : 1.1, 'Mana' : 13}]

def tri_poids(fournitures):
    for element in range(len(fournitures) - 1):
        # pour trier le tableau
        poids_minimal = element
        for element_min in range(element + 1 , len(fournitures)):
            if fournitures[element_min]['Poids'] >= \
                fournitures[poids_minimal]['Poids']:
                poids_minimal = element_min
        fournitures[element],fournitures[poids_minimal] = \
            fournitures[poids_minimal], fournitures[element]
    return fournitures

def malle_poids(fournitures):
    malle_trier = tri_poids(fournitures)
    malle = []
    poids_max = 4
    poids = 0
    for i in range(len(malle_trier)) :
        if malle_trier[i]['Poids'] + poids <= poids_max:
            malle.append({'Nom': malle_trier[i]['Nom'], 'Poids':\
                          malle_trier[i]['Poids']})
            poids += malle_trier[i]['Poids']
    return malle

malle_rempli = malle_poids(fournitures_scolaires)
poids_total = 0
for i in range(len(malle_rempli)):
    poids_total += malle_rempli[i]['Poids']

print(f"La malle pèse {poids_total} kg et contient :\n")
for i in range(len(malle_rempli)):
    print(f"{malle_rempli[i]['Nom']}")

