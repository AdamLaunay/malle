fournitures_scolaires = \
    [{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
     {'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
     {'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
     {'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
     {'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
     {'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
     {'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
     {'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
     {'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
     {'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]
'''
COMMENTAIRES
'''


def remplir_maille(fournitures):
    malle = []
    poids_max = 4

    for element in fournitures:
        if element['Poids'] <= poids_max:
            malle.append(element)
            poids_max -= element['Poids']

    return malle


maille_complete = remplir_maille(fournitures_scolaires)
print(maille_complete )
