# coding: utf-8


class styles:
    gras = '\33[1m'
    normal = '\33[m'
    souligner = '\033[4m'
    violet = '\033[35m'
    rouge = '\033[31m'
    vert = '\033[92m'
    rose = '\033[95m'
    bleu = '\033[34m'


fournitures_scolaires = \
    [{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
     {'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
     {'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
     {'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
     {'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
     {'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
     {'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
     {'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
     {'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
     {'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]
poids_max = 4


def malle_nimporte(fournitures):

    '''
    Cette fonction rempli de la maille n'importe comment tout en respectant la limite de poids.
    ENTREE: fournitures : table de dictionnaire avec toutes les fournitures,
            poids_max : entier, poids de malle a ne pas dépasser.
    SORTIE: malle : liste contenant les fournitures.
            poids_max : entier, poids de la malle a ne pas dépasser.
    '''

    malle = []
    poids_max = 4

    for element in fournitures:
        if element['Poids'] <= poids_max:
            malle.append(element)
            poids_max -= element['Poids']

    return malle


malle_rempli = malle_nimporte(fournitures_scolaires)
poids_total = 0
for i in range(len(malle_rempli)):
    poids_total += malle_rempli[i]['Poids']

def tri_poids(fournitures):
    for element in range(len(fournitures) - 1):
        # pour trier le tableau
        poids_minimal = element
        for element_min in range(element + 1, len(fournitures)):
            if fournitures[element_min]['Poids'] >= \
                    fournitures[poids_minimal]['Poids']:
                poids_minimal = element_min
        fournitures[element], fournitures[poids_minimal] = \
            fournitures[poids_minimal], fournitures[element]
    return fournitures


def malle_poids(fournitures):
    malle_trier = tri_poids(fournitures)
    malle = []
    poids_max = 4
    poids = 0
    for i in range(len(malle_trier)):
        if malle_trier[i]['Poids'] + poids <= poids_max:
            malle.append({'Nom': malle_trier[i]['Nom'], 'Poids':
            malle_trier[i]['Poids']})
            poids += malle_trier[i]['Poids']
    return malle


malle_rempli2 = malle_poids(fournitures_scolaires)
poids_total2 = 0
for i in range(len(malle_rempli2)):
    poids_total2 += malle_rempli2[i]['Poids']

def tri_mana(fournitures):
    for element in range(len(fournitures) - 1):
        # pour trier le tableau
        poids_minimal = element
        for element_min in range(element + 1, len(fournitures)):
            if fournitures[element_min]['Mana'] >= \
               fournitures[poids_minimal]['Mana']:
                poids_minimal = element_min
        fournitures[element], fournitures[poids_minimal] = \
            fournitures[poids_minimal], fournitures[element]
    return fournitures


def malle_mana(fournitures):
    malle_trier = tri_mana(fournitures)
    malle = []
    mana = 0
    poids = 0
    for i in range(len(malle_trier)):
        if malle_trier[i]['Poids'] + poids <= poids_max:
            malle.append({'Nom': malle_trier[i]['Nom'], 'Poids':
            malle_trier[i]['Poids'], 'Mana': malle_trier[i]['Mana']})
            poids += malle_trier[i]['Poids']
            mana += malle_trier[i]['Mana']
    return malle


malle_rempli3 = malle_mana(fournitures_scolaires)
poids_total3 = 0
mana_total = 0
for i in range(len(malle_rempli3)):
    poids_total3 += malle_rempli3[i]['Poids']
    mana_total += malle_rempli3[i]['Mana']


def liste_binaire(nb_fournitures):
    liste = []
    for i in range(2 ** nb_fournitures):
        nb_binaire = bin(i)[2:]
        nb_binaire = (nb_fournitures - len(nb_binaire)) * '0' + nb_binaire
        liste.append(nb_binaire)
    return liste


def liste_combinaisons(fournitures):
    binaires = liste_binaire(len(fournitures))
    combinaisons = []
    for nb_binaire in binaires:
        combinaison = []
        for i, bit in enumerate(nb_binaire):
            if bit == '1':
                combinaison.append(fournitures[i])
        combinaisons.append(combinaison)
    return combinaisons


def calcul_poids(combinaison):
    poids = 0
    for objet in combinaison:
        poids += objet['Poids']
    return poids


def meilleur_choix(liste_choix, poids_max):
    for choix in liste_choix:
        poids = calcul_poids(choix)
        if poids <= poids_max:
            top_choix = choix
    return top_choix


malle_force_brute = (meilleur_choix(liste_combinaisons(fournitures_scolaires), poids_max))


z = 1

while z == 1:
    print(f"{styles.gras}{styles.souligner}Quelle méthode voulez-vous utiliser pour remplir la malle :{styles.normal}")
    print(f"{styles.gras}{styles.vert} \n[a]: {styles.normal}Remplir n'importe comment")
    print(f"{styles.gras}{styles.vert}[b]: {styles.normal}Remplir le plus lourd possible")
    print(f"{styles.gras}{styles.vert}[c]: {styles.normal}Remplir avec le plus de valeur magique possible ")
    print(f"{styles.gras}{styles.vert}[d]: {styles.normal}Remplir avec la force brute ")
    reponse = (input())
    while reponse.lower() not in ['a', 'b', 'c', 'd']:
        print('Veuillez entrez une valeur valide :')
        reponse = (input())
    if reponse == 'a':
        print(f"{styles.bleu}Vous avez choisi de remplir la malle n'importe comment, voici la malle : {styles.normal}\n")
        for i in range(len(malle_rempli)):
            print(f"{styles.gras}{malle_rempli[i]['Nom']}{styles.normal}: {styles.vert}{malle_rempli[i]['Poids']}{styles.normal} kg")
        print(f"\nLa malle pèse {styles.rouge}{poids_total}{styles.normal} kg.\n")
    elif reponse == 'b':
        print(f"{styles.bleu}Vous avez choisi de la remplir la plus lourde possible, voici la malle : {styles.normal}\n")
        for i in range(len(malle_rempli2)):
            print(f"{styles.gras}{malle_rempli2[i]['Nom']}{styles.normal}: {styles.vert}{malle_rempli2[i]['Poids']}{styles.normal} kg")
        print(f"\nLa malle pèse {styles.rouge}{poids_total2}{styles.normal} kg.\n")
    elif reponse == 'c':
        print(
            f"{styles.bleu}Vous avez choisi de remplir la malle avec le plus de valeur magique possible, voici la malle : {styles.normal}\n")
        for i in range(len(malle_rempli3)):
            print(f"{styles.gras}{malle_rempli3[i]['Nom']}{styles.normal}: {styles.rose}{malle_rempli3[i]['Mana']}{styles.normal} de mana, {styles.normal}{styles.vert}{malle_rempli3[i]['Poids']}{styles.normal} kg")
        print(f"\nLa malle a pour valeur magique {styles.gras}{styles.violet}{mana_total}{styles.normal} et pèse {styles.gras}{styles.rouge}{poids_total3}{styles.normal} kg.")
    elif reponse == 'd':
        print(f"{styles.bleu}Vous avez choisi la force brute, voici la malle : {styles.normal}\n")
        for element in malle_force_brute:
            print(f"{styles.gras}{element['Nom']}{styles.normal}: {styles.vert}{element['Poids']}{styles.normal} kg")
        print(f"\nLa malle pèse {styles.rouge}{poids_total}{styles.normal} kg.\n")

    print(f"{styles.gras}{styles.souligner}\nVoulez-vous à nouveau remplir la malle ? :{styles.normal}")
    print(f"{styles.gras}{styles.vert} \n[o]: {styles.normal}Oui")
    print(f"{styles.gras}{styles.vert}[n]: {styles.normal}Non")
    rep1 = input()
    while rep1.lower() not in ['o', 'n']:
        print('Veuillez entrez une valeur valide :')
        rep = (input())
    if rep1 == 'o':
        pass
    else:
        print(f"{styles.gras}Au revoir !")
        z += 1
